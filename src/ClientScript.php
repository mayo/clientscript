<?php
/*  
	Mayo Engine by John Wood
	Not for use without permission
	watch_wood@hotmail.com
*/
namespace Mayo;

/** Handles the loading of common js/css/etc for the front end site.
 * The class effectively acts as a struct, controlled by the various static 
 * function calls available to it.  Allows for easy on-demand loading of 
 * common front end libraries such as jquery, fontawesome, etc
 */
class ClientScript {
	static protected $list = array();//!< array of ClientScripts available
	
	/** Adds a JS file to the dependancy stack
		@param name the name of the library
		@param file the file to add to the stack
	*/
	static function jsFile ($name, $file){
		return static::add($name, '<script src="'.$file.'"></script>', "footer");
	}
	
	/** Adds a block of JS code to the dependancy stack
		@param name the name of the library
		@param file the file to add to the stack
	*/
	static function jsCode ($name, $code){
		return static::add($name, '<script>'.$code.'</script>', "footer");
	}
	
	/** Adds a CSS file to the dependancy stack.
		@param name the name of the library
		@param file the file to add to the stack
	*/
	static function cssFile ($name, $file){
		return static::add($name, '<link rel="stylesheet" type="text/css" href="'.$file.'">');
	}
	
	/** adds a block of CSS code to the dependancy stack
		@param name the name of the library
		@param file the file to add to the stack
	*/
	static function cssCode ($name, $code){
		return static::add($name, '<style>'.$code.'</style>');
	}
	
	/** adds a generic block of text dependancy stack
		@param name the name of the library
		@param code the code to add to the stack
		@param location the location in the rendered page to add to
	*/
	static function add ($name, $file, $location = "header"){
		$class = get_called_class();
		$script = new $class($name, $file, $location);
		static::$list[$name] = &$script;
		return $script;
	}
	
	/** requests that a given library be added to the render stack
		@param name the name of the library
	*/
	static function request ($name){
		//prevent infinite loop
		if (static::$list[$name]->requested) 
			return; 
		
		//grab library and all requirements
		static::$list[$name]->requested = true;
		foreach (static::$list[$name]->requirements as $req){
			static::request($req);
		}
	}
	
	/** renders the requested code to the HTML layout
		@param layout the layout object to render the libaries to
	*/
	static function defaults (){
		static::jsFile("angular", "//ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular.min.js");
		static::jsFile("jquery", "//code.jquery.com/jquery-1.11.3.min.js");
		static::jsFile("underscore", "https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js");
		static::cssFile("fontawesome", "//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css");
	}
	
	/** renders the requested objects.
		@param location location to render for
	*/
	static function render ($location){
		$output = array();
		foreach (static::$list as $item){
			if ($item->requested && $location==$item->location){
				array_push($output, $item->content);
			}
		}
		return implode($output);
	}
	
	/** renders the requested header code (typically CSS).
	*/
	static function renderHeader (){
		return static::render("header");
	}
	
	/** renders the requested footer code (typically JS).
	*/
	static function renderFooter (){
		return static::render("footer");
	}
	
	public $name = "";//!< name of the library object
	public $content = "";//!< code/link of the library object
	public $location = "";//!< location in the layout to add the code/call to
	public $requested = false;//!< true/false, if the script should be included in the page render
	public $requirements = array();//!< true/false, if the script should be included in the page render
	
	/** constructor for a ClientScript object
		@param name the name of the library
		@param code the code to add to the stack
		@param location the location in the rendered page to add to
	*/
	function __construct ($name, $content, $location="header"){
		$this->name = $name;
		$this->content = $content;
		$this->location = $location;
		return $this;
	}
	
	/** adds dependancies to library (ie, jQuery for a jQuery plugin)
		@param params string/array name(s) of plugins to require
	*/
	function requires ($params){
		if (is_array($params))
			$this->requirements = $this->requirements + $params;
		else 
			$this->requirements[] = $params;
	}
}

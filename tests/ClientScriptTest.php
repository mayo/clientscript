<?php

use Mayo\ClientScript;

//makes the scripts list resettable for easy testing
class Script extends ClientScript {
	static public $list = array();
	static public function reset () {
		static::$list = array();
	}
}

class ClientScriptTest extends PHPUnit_Framework_TestCase {
	protected $name = "TestName";
	protected $content = "Test Content";

	public function testCreation () {
		$script = new Script($this->name, $this->content);
		$this->assertEquals($script->name, $this->name);
		$this->assertEquals($script->content, $this->content);
		$this->assertEquals($script->location, "header");
	}
	
	public function testDependancies1(){
		$script = new Script($this->name, $this->content);
		$script->requires("helper");
		$this->assertEquals($script->requirements, ["helper"]);
	}
	
	public function testDependancies2(){
		$script = new Script($this->name, $this->content);
		$script->requires(["helper1", "helper2"]);
		$this->assertEquals(count($script->requirements), 2);
	}
	
	
	public function testStack(){
		Script::reset();
		Script::add($this->name, $this->content);
		Script::request($this->name);
		$output = Script::render("header");
		$this->assertEquals($this->content, $output);
	}
	
	public function testJsFile(){
		Script::reset();
		Script::jsFile($this->name, $this->content);
		Script::request($this->name);
		$this->assertEquals(Script::renderFooter(), '<script src="'.$this->content.'"></script>');
	}
	
	public function testJsCode(){
		Script::reset();
		Script::jsCode($this->name, $this->content);
		Script::request($this->name);
		$this->assertEquals(Script::renderFooter(), '<script>'.$this->content.'</script>');
	}
	
	public function testCssFile(){
		Script::reset();
		Script::cssFile($this->name, $this->content);
		Script::request($this->name);
		$this->assertEquals(Script::renderHeader(), '<link rel="stylesheet" type="text/css" href="'.$this->content.'">');
	}
	
	public function testCssCode(){
		Script::reset();
		Script::cssCode($this->name, $this->content);
		Script::request($this->name);
		$this->assertEquals(Script::renderHeader(), '<style>'.$this->content.'</style>');
	}
	
}
